#!/bin/bash

# variables
PART='knlq'
NTASKS=1
NPROC=1
OUTPUTFILE='/home/lbl870/sample_query/output/%A.out'
ERRORFILE='/home/lbl870/sample_query/errors/%A.err'
TIME='36:00:00'
BEGIN='now'

JOB="$1"
WORKDIR='/home/lbl870/sample_query'

if [ ! -d "$HOME/scratch/pythonenvs/sample_query" ]
then
  mkdir $HOME/scratch/pythonenvs/sample_query
  cd $HOME/scratch/pythonenvs/sample_query
  module load python/3.6.0
  python3 -m venv env
  env/bin/pip install -r /home/lbl870/sample_query/requirements.txt
  cd '/home/lbl870/sample_query'
fi

sbatch  --error="$ERRORFILE"\
        --output="$OUTPUTFILE"\
        --ntasks="$NTASKS"\
        --ntasks-per-node="$NPROC"\
        --time="$TIME"\
        --begin="$BEGIN"\
        --partition="$PART"\
        --workdir=$WORKDIR/\
        --job-name='datcreat'\
        "$JOB"

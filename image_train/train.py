import os
import torch
import time
import copy
import wandb
from torch import nn, optim
from torchvision import transforms, datasets, models
from PIL import Image, ImageFile

import warnings
warnings.filterwarnings("ignore", category=UserWarning)

Image.MAX_IMAGE_PIXELS = 142606336
ImageFile.LOAD_TRUNCATED_IMAGES = True


def get_dataloader(data_dir):
    data_transforms = {
        'train': transforms.Compose([
            transforms.RandomResizedCrop(224),
            transforms.RandomHorizontalFlip(),
            transforms.ToTensor(),
            transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
        ]),
        'val': transforms.Compose([
            transforms.Resize(256),
            transforms.CenterCrop(224),
            transforms.ToTensor(),
            transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
        ]),
        'test': transforms.Compose([
            transforms.Resize(256),
            transforms.CenterCrop(224),
            transforms.ToTensor(),
            transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
        ])
    }
    image_datasets = {x: datasets.ImageFolder(root=os.path.join(data_dir, x),
                                              transform=data_transforms[x])
                      for x in ['train', 'val', 'test']}

    dataset_loader = {x: torch.utils.data.DataLoader(image_datasets[x],
                                                     batch_size=32,
                                                     shuffle=True,
                                                     num_workers=4)
                      for x in ['train', 'val', 'test']}

    dataset_sizes = {x: len(image_datasets[x]) for x in ['train', 'val', 'test']}
    class_names = image_datasets['train'].classes

    return dataset_loader, dataset_sizes, class_names


def train_model(model, criterion, optimizer, dataset_loaders, dataset_sizes, device, num_epochs=25):
    since = time.time()

    best_model_wts = copy.deepcopy(model.state_dict())
    best_acc = 0.0

    for epoch in range(num_epochs):
        epoch_time = time.time()
        print('Epoch {}/{}'.format(epoch, num_epochs - 1))
        print('-' * 10)

        stats = {"train_acc": 0.0,
                 "train_loss": 0.0,
                 "val_acc": 0.0,
                 "val_loss": 0.0}

        for phase in ['train', 'val']:
            if phase == 'train':
                model.train()
            else:
                model.eval()

            running_loss = 0.0
            running_corrects = 0

            for inputs, labels in dataset_loaders[phase]:
                inputs = inputs.to(device)
                labels = labels.to(device)

                optimizer.zero_grad()

                with torch.set_grad_enabled(phase == 'train'):
                    outputs = model(inputs)
                    _, preds = torch.max(outputs, 1)
                    loss = criterion(outputs, labels)

                    if phase == 'train':
                        loss.backward()
                        optimizer.step()

                running_loss += loss.item() * inputs.size(0)
                running_corrects += torch.sum(preds == labels.data)

            epoch_loss = running_loss / dataset_sizes[phase]
            epoch_acc = running_corrects.double() / dataset_sizes[phase]

            print('{} Loss: {:.4f} Acc: {:.4f}'.format(phase, epoch_loss, epoch_acc))

            stats["{}_acc".format(phase)] = epoch_acc
            stats["{}_loss".format(phase)] = epoch_loss

            if phase == 'val' and epoch_acc > best_acc:
                best_acc = epoch_acc
                best_model_wts = copy.deepcopy(model.state_dict())

        time_elapsed = time.time() - epoch_time
        print('Epoch complete in {:.0f}m {:.0f}s'.format(time_elapsed // 60, time_elapsed % 60))
        print()

        wandb.log({"Train Accuracy": stats["train_acc"],
                   "Train Loss": stats["train_loss"],
                   "Val Accuracy": stats["val_acc"],
                   "Val Loss": stats["val_loss"]})

    time_elapsed = time.time() - since
    print('Training complete in {:.0f}m {:.0f}s'.format(time_elapsed // 60, time_elapsed % 60))
    print('Best val Acc: {:4f}'.format(best_acc))

    model.load_state_dict(best_model_wts)

    return model


def test_model(model, criterion, optimizer, dataset_loaders, dataset_sizes, device):
    since = time.time()

    model.eval()

    running_loss = 0.0
    running_corrects = 0

    for inputs, labels in dataset_loaders['test']:
        inputs = inputs.to(device)
        labels = labels.to(device)

        optimizer.zero_grad()

        with torch.set_grad_enabled(False):
            outputs = model(inputs)
            _, preds = torch.max(outputs, 1)
            loss = criterion(outputs, labels)

        running_loss += loss.item() * inputs.size(0)
        running_corrects += torch.sum(preds == labels.data)

    loss = running_loss / dataset_sizes['test']
    acc = running_corrects.double() / dataset_sizes['test']

    print('{} Loss: {:.4f} Acc: {:.4f}'.format('test', loss, acc))

    time_elapsed = time.time() - since
    print('Test evaluation complete in {:.0f}m {:.0f}s'.format(time_elapsed // 60, time_elapsed % 60))
    print()

    wandb.log({"Test Accuracy": acc,
               "Test Loss": loss})


if __name__ == "__main__":
    objective = "Gender"
    total_train = True
    num_epochs = 100
    learn_rate = 0.001

    images_dir = "/home/lbl870/scratch/triples/split_genre"
    if objective == "Genre":
        images_dir = "/home/lbl870/scratch/triples/split_genre"
    elif objective == "Gender":
        images_dir = "/home/lbl870/scratch/triples/split_gender"

    dataset_loader, dataset_sizes, class_names = get_dataloader(images_dir)
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

    print("Retraining the entire resnet18: {}".format(total_train))
    print("Training folder: {}\nOn device: {}".format(images_dir, device))
    print("Size of dataset;\tTrain: {}\tVal: {}\tTest: {}".format(len(dataset_loader['train'].dataset),
                                                                  len(dataset_loader['val'].dataset),
                                                                  len(dataset_loader['test'].dataset)))

    model = models.resnet18(pretrained=True)
    if not total_train:
        for param in model.parameters():
            param.requires_grad = False

    num_ftrs = model.fc.in_features

    model.fc = nn.Linear(num_ftrs, len(class_names))

    model.to(device)
    criterion = nn.CrossEntropyLoss()
    optimizer = optim.Adam(model.parameters(), lr=learn_rate)

    wandb.init(project="image_train",
               config={"Train whole resnet18": total_train,
                       "Objective": objective,
                       "Epochs": num_epochs,
                       "Learn_rate": learn_rate,
                       "Num_param": sum(p.numel() for p in model.parameters() if p.requires_grad)
                       })
    wandb.watch(model)

    best_model = train_model(model, criterion, optimizer, dataset_loader, dataset_sizes, device, num_epochs)

    test_model(model, criterion, optimizer, dataset_loader, dataset_sizes, device)

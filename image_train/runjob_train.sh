#!/bin/bash

# variables
PART='defq'
CONSTRAINT='gpunode'
NTASKS=5
NPROC=1
OUTPUTFILE='/home/lbl870/image_train/output/%A.out'
ERRORFILE='/home/lbl870/image_train/errors/%A.err'
TIME='9:00:00'
BEGIN='now'

JOB="$1"
WORKDIR='/home/lbl870/image_train/scripts'/$(date +%Y%m%d%H%M%S)
export TORCH_MODEL_ZOO=/home/lbl870/scratch/pythonenvs/image_train

mkdir -p $WORKDIR
cp train.py $WORKDIR

if [ ! -d "$HOME/scratch/pythonenvs/image_train" ]
then
  mkdir $HOME/scratch/pythonenvs/image_train
  cd $HOME/scratch/pythonenvs/image_train
  module load python/3.6.0
  python3 -m venv env
  env/bin/pip install -r /home/lbl870/image_train/requirements.txt
  cd '/home/lbl870/image_train'
fi

sbatch  --error="$ERRORFILE"\
        --output="$OUTPUTFILE"\
        --ntasks="$NTASKS"\
        --ntasks-per-node="$NPROC"\
        --time="$TIME"\
        --begin="$BEGIN"\
        --partition="$PART"\
        --constraint="$CONSTRAINT"\
        --workdir=$WORKDIR/\
        --job-name='image_train'\
        "$JOB"

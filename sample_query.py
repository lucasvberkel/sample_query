import requests
import rdflib
import time
import json
import os
import base64
import random
import distutils.util
import numpy as np

from copy import deepcopy
from collections import Counter
from SetCoverPy import setcover
from rdflib import URIRef, Graph, Literal, Namespace
from io import BytesIO
from PIL import Image, ImageOps, ImageFile, UnidentifiedImageError
from bs4 import BeautifulSoup
from shutil import copyfile


headers = {'User-Agent': 'bot for generating knowledge graph', 'From': 'lucasvberkel@gmail.com'}

wd = Namespace('http://www.wikidata.org/entity/')
wdt = Namespace('http://www.wikidata.org/prop/direct/')

Image.warnings.simplefilter('ignore', UserWarning)
Image.MAX_IMAGE_PIXELS = 163577856
ImageFile.LOAD_TRUNCATED_IMAGES = True


def query_wikidata(query: str, timeout: int = 60):
    response = requests.get("https://query.wikidata.org/sparql",
                            headers=headers,
                            params={'format': 'json', 'query': query},
                            timeout=timeout)
    data = response.json()
    return data['results']['bindings']


def get_imdb_data(imdb_identifier: str, type_of_id="film"):
    url = "{}"
    if type_of_id == "film":
        url = "https://www.imdb.com/title/{}/"
    elif type_of_id == "person":
        url = "https://www.imdb.com/name/{}/"
    response = requests.get(url.format(imdb_identifier),
                            headers=headers,
                            timeout=60)

    obj = BeautifulSoup(response.content,
                        features="html.parser")
    json_object = json.loads(obj.find('script',
                                      type="application/ld+json").string)

    image_link = json_object["image"]
    if "aggregateRating" in json_object:
        rating = float(json_object["aggregateRating"]["ratingValue"])
        rating = str(round(rating * 2) / 2)
    else:
        rating = "Undefined"
    return image_link, rating


def get_image(link: str):
    response = requests.get(link, headers=headers, stream=True, timeout=60)
    if response.status_code != 200:
        raise requests.exceptions.ConnectionError
    return response


def add_triples(graph: Graph, triples: list):
    for triple in triples:
        if not distutils.util.strtobool(triple['literal']['value']):
            graph.add((URIRef(triple['s']['value']),
                       URIRef(triple['p']['value']),
                       URIRef(triple['o']['value'])))
        else:
            graph.add((URIRef(triple['s']['value']),
                       URIRef(triple['p']['value']),
                       Literal(triple['o']['value'], datatype=triple['datatype']['value'])))
    return graph


def query_graph(graph: Graph, query: str):
    return graph.query(query, initNs={"wd": wd, "wdt": wdt})


def update_graph(graph: Graph, query: str):
    graph.update(query, initNs={"wd": wd, "wdt": wdt})
    return graph


def gather_individuals(movie_data_list: list):
    persons_set = set()
    print("Gathering individuals from {} movies".format(len(movie_data_list)))
    for single_movie in movie_data_list:
        query_movie = query_all_persons.format(single_movie['uri']['value'].split('/')[-1], allowed_relations)

        try:
            relation_data = query_wikidata(query_movie)
        except requests.exceptions.Timeout:
            print("TIMED OUT WITH MOVIE {}".format(single_movie['uri']['value'].split('/')[-1]))
            continue
        except json.decoder.JSONDecodeError:
            print("JSON ERROR WITH MOVIE {}".format(single_movie['uri']['value'].split('/')[-1]))
            continue

        for rel in relation_data:
            persons_set.add("{}".format(rel['o']['value']))
    return persons_set


def gather_movie_triples(graph: Graph, movie_data_list: list):
    print("Gathering triples from {} movies".format(len(movie_data_list)))
    for single_movie in movie_data_list:
        # Scrape all triples
        query_movie_triples = query_all_triples.format(single_movie['uri']['value'].split('/')[-1], allowed_relations)

        try:
            movie_triples = query_wikidata(query_movie_triples)
        except requests.exceptions.Timeout:
            print("TIMED OUT WITH MOVIE {}".format(single_movie['uri']['value'].split('/')[-1]))
            continue
        except json.decoder.JSONDecodeError:
            print("JSON ERROR WITH MOVIE {}".format(single_movie['uri']['value'].split('/')[-1]))
            continue

        # Add as instance of movie
        graph.add((URIRef(wd[single_movie['uri']['value'].split('/')[-1]]),
                   URIRef(wdt["P31"]),
                   URIRef(wd["Q11424"])))

        graph = add_triples(graph, movie_triples)
    return graph


def gather_person_triples(graph: Graph, persons_set: set):
    print("Gathering triples from {} persons".format(len(persons_set)))
    # for person in persons_set:
    for person in persons_set:
        # Scrape all persons
        query_person_triples = query_all_triples.format(person.split('/')[-1], allowed_relations)

        try:
            person_triples = query_wikidata(query_person_triples)
        except requests.exceptions.Timeout:
            print("TIMED OUT WITH PERSON {}".format(person.split('/')[-1]))
            continue
        except json.decoder.JSONDecodeError:
            print("JSON ERROR WITH PERSON {}".format(person.split('/')[-1]))
            continue

        # Add as instance of human
        graph.add((URIRef(wd[person.split('/')[-1]]),
                   URIRef(wdt["P31"]),
                   URIRef(wd["Q5"])))

        graph = add_triples(graph, person_triples)
    return graph


def solve_scp_problem(graph: Graph):
    movie_query_pairs = query_graph(graph, query="SELECT ?s ?genre WHERE { ?s wdt:P31 wd:Q11424 . ?s wdt:P136 ?genre }")

    genre_counter = Counter()
    node_counter = Counter()
    genre_set = set()
    for row in movie_query_pairs.bindings:
        genre_counter[row[rdflib.term.Variable('genre')]] += 1
        node_counter[row[rdflib.term.Variable('s')]] += 1
        genre_set.add(row[rdflib.term.Variable('genre')].split("/")[-1])

    n2i = {}
    i2n = {}
    for i, (x, count) in enumerate(genre_counter.most_common()):
        n2i[x] = i
        i2n[i] = x

    n2i_films = {}
    i2n_films = {}
    for i, (x, count) in enumerate(node_counter.most_common()):
        n2i_films[x] = i
        i2n_films[i] = x

    a_matrix = np.zeros(shape=(len(n2i_films), len(n2i)), dtype=int)
    for row in movie_query_pairs.bindings:
        a_matrix[n2i_films[row[rdflib.term.Variable('s')]], n2i[row[rdflib.term.Variable('genre')]]] = 1

    cost = np.reciprocal(a_matrix.sum(axis=0, dtype=np.float64))

    scp_solution = setcover.SetCover(a_matrix, cost, maxiters=100)
    _, __ = scp_solution.SolveSCP()

    movie_to_genre = {}
    for row in movie_query_pairs.bindings:
        if not row[rdflib.term.Variable('s')] in movie_to_genre:
            movie_to_genre[row[rdflib.term.Variable('s')]] = set()
        movie_to_genre[row[rdflib.term.Variable('s')]].add(row[rdflib.term.Variable('genre')])

    movie_per_genre_counter = Counter()
    for idx in scp_solution.s.nonzero()[0]:
        key_list = list(movie_to_genre.keys())
        for movie in key_list:
            if i2n[idx] in movie_to_genre[movie]:
                movie_per_genre_counter[idx] += 1
                del movie_to_genre[movie]

    accepted_genres = set()
    for x, count in movie_per_genre_counter.most_common():
        if count > 50:
            accepted_genres.add(x)
    accepted_indices = scp_solution.s.nonzero()[0]
    return accepted_indices, i2n, n2i, genre_set, accepted_genres


def removing_redundant_genres(graph: Graph, accepted_indices, i2n: dict, genre_set: set, accepted_genres: set):
    query_movie_genre = "SELECT ?s WHERE {{ bind(wd:{} as ?genre) . ?s wdt:P31 wd:Q11424 . ?s wdt:P136 ?genre }}"
    query_delete_genre = "DELETE DATA {{ wd:{} wdt:P136 wd:{} . }}"
    query_insert_genre = "INSERT DATA {{ wd:{} wdt:P136 wd:{} }}"
    print("Removing redundant genres from {} genres".format(len(accepted_indices)))
    for idx in accepted_indices:
        genre = i2n[idx].split("/")[-1]
        movie_with_certain_genre = query_graph(graph, query=query_movie_genre.format(genre))
        for row in movie_with_certain_genre.bindings:
            movie = row[rdflib.term.Variable('s')].split("/")[-1]
            graph = update_graph(graph, query=query_delete_genre.format(movie, " , wd:".join(genre_set)))
            if idx in accepted_genres:
                graph = update_graph(graph, query=query_insert_genre.format(movie, genre))
            else:
                graph = update_graph(graph, query=query_insert_genre.format(movie, "Q2973158"))
    return graph


def removing_other_genres(graph: Graph):
    query_other_entities = "SELECT ?s ?g WHERE { ?s wdt:P136 ?g . filter not exists { ?s wdt:P31 wd:Q11424 } }"
    query_delete_other = "DELETE DATA {{ wd:{} wdt:P136 wd:{} . }}"
    other_entities_with_genres = query_graph(graph, query=query_other_entities)
    print("Removing other genres from {} other entities".format(len(other_entities_with_genres.bindings)))
    for row in other_entities_with_genres.bindings:
        graph = update_graph(graph, query=query_delete_other.format(row[rdflib.term.Variable('s')].split("/")[-1],
                                                                    row[rdflib.term.Variable('g')].split("/")[-1]))
    return graph


def film_poster_extraction(graph: Graph, root_folder: str):
    query_imdb_id = "SELECT ?s ?genre ?imdb_id WHERE { ?s wdt:P136 ?genre . ?s wdt:P345 ?imdb_id . }"
    query_insert_image = "INSERT DATA {{ wd:{} wdt:{} <{}> }}"
    datatype = URIRef("http://www.w3.org/2001/XMLSchema#b64string")
    error_photos = set()
    movie_genre_imdb_id = query_graph(graph, query_imdb_id)
    print("Extracting film poster from {} movies".format(len(movie_genre_imdb_id.bindings)))
    for row in movie_genre_imdb_id.bindings:
        movie = row[rdflib.term.Variable('s')].split("/")[-1]
        genre = row[rdflib.term.Variable('genre')].split("/")[-1]
        imdb_id = row[rdflib.term.Variable('imdb_id')].split("/")[-1]

        if not os.path.exists(os.path.join(root_folder, genre)):
            os.makedirs(os.path.join(root_folder, genre))
        image_folder = os.path.join(root_folder, genre)

        try:
            hyperlink, _ = get_imdb_data(imdb_id, type_of_id="film")
            image_end = hyperlink.split(".")[-1]

            image = get_image(hyperlink)

            graph = update_graph(graph, query_insert_image.format(movie, "P3383", hyperlink))
        except (requests.exceptions.Timeout, requests.exceptions.ConnectionError, ValueError, BaseException):
            error_photos.add(movie)
            continue

        with open("{}/{}_full.{}".format(image_folder, movie, image_end), 'wb') as image_file:
            for chunk in image:
                image_file.write(chunk)

        try:
            image_pil = Image.open("{}/{}_full.{}".format(image_folder, movie, image_end)).convert("RGB")
        except UnidentifiedImageError:
            continue

        image_pil.thumbnail(size=(64, 64), resample=Image.ANTIALIAS)
        image_pil = ImageOps.pad(image_pil, size=(64, 64))
        image_pil.save("{}/{}.png".format(image_folder, movie), format="PNG")

        with open("{}/{}.nt".format(image_folder, movie), 'w') as image_file:
            output = BytesIO()
            image_pil.save(output, format="png")
            image_file.write("{}\n".format(Literal(base64.b64encode(output.getvalue()),
                                                   datatype=datatype).n3()))
    print("Errors with {} movies".format(len(error_photos)))

    return graph


def individual_photo_extraction(graph: Graph, root_folder: str):
    query_imdb_id_persons = "SELECT ?s ?gender ?imdb_id WHERE { ?s wdt:P21 ?gender . ?s wdt:P345 ?imdb_id . }"
    query_insert_image = "INSERT DATA {{ wd:{} wdt:{} <{}> }}"
    datatype = URIRef("http://www.w3.org/2001/XMLSchema#b64string")
    error_photos = set()
    persons_gender_imdb_id = query_graph(graph, query_imdb_id_persons)
    print("Extracting images from {} persons".format(len(persons_gender_imdb_id.bindings)))
    for row in persons_gender_imdb_id.bindings:
        person = row[rdflib.term.Variable('s')].split("/")[-1]
        gender = row[rdflib.term.Variable('gender')].split("/")[-1]
        imdb_id = row[rdflib.term.Variable('imdb_id')].split("/")[-1]

        if not os.path.exists(os.path.join(root_folder, gender)):
            os.makedirs(os.path.join(root_folder, gender))
        image_folder = os.path.join(root_folder, gender)

        try:
            hyperlink, _ = get_imdb_data(imdb_id, type_of_id="person")
            image_end = hyperlink.split(".")[-1]

            image = get_image(hyperlink)

            graph = update_graph(graph, query_insert_image.format(person, "P18", hyperlink))
        except (requests.exceptions.Timeout, requests.exceptions.ConnectionError, ValueError, BaseException):
            error_photos.add(person)
            continue

        with open("{}/{}_full.{}".format(image_folder, person, image_end), 'wb') as image_file:
            for chunk in image:
                image_file.write(chunk)

        try:
            image_pil = Image.open("{}/{}_full.{}".format(image_folder, person, image_end)).convert("RGB")
        except UnidentifiedImageError:
            continue

        image_pil.thumbnail(size=(64, 64), resample=Image.ANTIALIAS)
        image_pil = ImageOps.pad(image_pil, size=(64, 64))
        image_pil.save("{}/{}.png".format(image_folder, person), format="PNG")

        with open("{}/{}.nt".format(image_folder, person), 'w') as image_file:
            output = BytesIO()
            image_pil.save(output, format="png")
            image_file.write("{}\n".format(Literal(base64.b64encode(output.getvalue()),
                                                   datatype=datatype).n3()))
    print("Errors with {} persons".format(len(error_photos)))

    return graph


def add_images_to_graph(graph: Graph):
    query_all_images = "SELECT ?s ?o ?image WHERE { ?s wdt:P18|wdt:P3383|wdt:P154 ?image . ?s ?o ?image . }"
    query_delete_image_link = "DELETE DATA {{ wd:{} wdt:{} <{}> . }}"
    query_insert_image = "INSERT DATA {{ wd:{} wdt:{} {} }}"
    datatype = URIRef("http://www.w3.org/2001/XMLSchema#b64string")
    image_in_graph = query_graph(graph, query_all_images)
    error_photos = set()
    print("Adding {} images to the graph".format(len(image_in_graph.bindings)))
    for row in image_in_graph.bindings:
        hyperlink = str(row[rdflib.term.Variable('image')])

        try:
            image = get_image(hyperlink)
        except (requests.exceptions.Timeout, requests.exceptions.ConnectionError):
            error_photos.add(hyperlink)
            continue

        try:
            image_pil = Image.open(BytesIO(image.content)).convert("RGB")
        except UnidentifiedImageError:
            continue

        image_pil.thumbnail(size=(64, 64), resample=Image.ANTIALIAS)
        image_pil = ImageOps.pad(image_pil, size=(64, 64))

        output = BytesIO()
        image_pil.save(output, format="png")

        graph = update_graph(graph, query_delete_image_link.format(row[rdflib.term.Variable('s')].split("/")[-1],
                                                                   row[rdflib.term.Variable('o')].split("/")[-1],
                                                                   row[rdflib.term.Variable('image')]))

        graph = update_graph(graph, query_insert_image.format(row[rdflib.term.Variable('s')].split("/")[-1],
                                                              row[rdflib.term.Variable('o')].split("/")[-1],
                                                              Literal(base64.b64encode(output.getvalue()),
                                                                      datatype=datatype).n3()))
    print("Errors with {} movies".format(len(error_photos)))
    return graph


def remove_imdb_id(graph: Graph):
    query_all_imdb_id = "SELECT ?s ?id WHERE { ?s wdt:P345 ?id }"
    query_delete_imdb_id = "DELETE DATA {{ wd:{} wdt:P345 \"{}\"^^<http://www.w3.org/2001/XMLSchema#string> . }}"
    imdb_id_in_graph = query_graph(graph, query_all_imdb_id)
    print("Removing {} imdb id from knowledge graph".format(len(imdb_id_in_graph.bindings)))
    print("Started with length {}".format(len(graph)))
    for row in imdb_id_in_graph.bindings:
        graph = update_graph(graph, query=query_delete_imdb_id.format(row[rdflib.term.Variable('s')].split("/")[-1],
                                                                      row[rdflib.term.Variable('id')].split("/")[-1]))
    print("Ended with length {}".format(len(graph)))
    return graph


def create_training_validation_test_set(graph: Graph, root_folder, movie_folder, person_folder):
    genre = {"objective": "P136",
             "size_val": 500.0,
             "size_test": 3000.0,
             "target_directory": "split_genre"}
    gender = {"objective": "P21",
              "size_val": 1000.0,
              "size_test": 10000.0,
              "target_directory": "split_gender"}

    for characteristics, folder in zip([genre, gender], [movie_folder, person_folder]):
        objective = characteristics["objective"]
        size_val = characteristics["size_val"]
        size_test = characteristics["size_test"]

        target_directory = os.path.join(root_folder, characteristics["target_directory"])
        if not os.path.exists(target_directory):
            os.makedirs(target_directory)
        if not os.path.exists(os.path.join(target_directory, "train")):
            os.makedirs(os.path.join(target_directory, "train"))
        if not os.path.exists(os.path.join(target_directory, "val")):
            os.makedirs(os.path.join(target_directory, "val"))
        if not os.path.exists(os.path.join(target_directory, "test")):
            os.makedirs(os.path.join(target_directory, "test"))

        query_total = "SELECT ?s ?o WHERE {{ ?s wdt:{} ?o }}".format(objective)
        query_classes = "SELECT DISTINCT ?o WHERE {{ ?_ wdt:{} ?o }}".format(objective)

        test_percentage = size_test / len(query_graph(graph, query_total))
        val_percentage = size_val / len(query_graph(graph, query_total))
        train_percentage = 1.0 - (test_percentage + val_percentage)

        train_graph = Graph()
        val_graph = Graph()
        test_graph = Graph()

        train_set = set()
        val_set = set()
        test_set = set()

        classes = [x[rdflib.term.Variable('o')].split("/")[-1] for x in query_graph(graph, query_classes).bindings]
        for category in classes:
            query_on_class = "SELECT ?s WHERE {{ ?s wdt:{} wd:{} }}"
            response = query_graph(graph, query=query_on_class.format(objective, category))
            for row in response.bindings:
                element = row[rdflib.term.Variable('s')].split("/")[-1]
                split_set = random.choices(population=["train_set", "val_set", "test_set"],
                                           weights=(train_percentage, val_percentage, test_percentage),
                                           k=1)[0]

                insert_query = "INSERT DATA {{ wd:{} wdt:{} wd:{} }}"
                delete_query = "DELETE DATA {{ wd:{} wdt:{} wd:{} }}"

                graph = update_graph(graph, delete_query.format(element, objective, category))

                if split_set == "train_set":
                    train_set.add(element)
                    train_graph = update_graph(train_graph, insert_query.format(element, objective, category))
                elif split_set == "val_set":
                    val_set.add(element)
                    val_graph = update_graph(val_graph, insert_query.format(element, objective, category))
                elif split_set == "test_set":
                    test_set.add(element)
                    test_graph = update_graph(test_graph, insert_query.format(element, objective, category))

        for path, sub_dirs, files in os.walk(folder):
            for name in files:
                element = name.split(".")[0].split("_")[0]
                phase = "train"
                if element in train_set:
                    phase = "train"
                elif element in val_set:
                    phase = "val"
                elif element in test_set:
                    phase = "test"

                assigned_class = path.split("/")[-1]

                file_directory = os.path.join(os.path.join(target_directory, phase), assigned_class)
                if not os.path.exists(file_directory):
                    os.makedirs(file_directory)

                copyfile(os.path.join(path, name), os.path.join(file_directory, name))
                os.remove(os.path.join(path, name))

        train_graph.serialize(destination=os.path.join(target_directory, "train.nt"), format="nt")
        val_graph.serialize(destination=os.path.join(target_directory, "val.nt"), format="nt")
        test_graph.serialize(destination=os.path.join(target_directory, "test.nt"), format="nt")


if __name__ == "__main__":
    with open('./queries/query_gather_all_movies.txt', 'r') as f:
        query_all_movies = f.read()

    with open('./queries/query_gather_all_persons.txt', 'r') as f:
        query_all_persons = f.read()

    with open('queries/allowed_relation.txt', 'r') as f:
        allowed_relations = f.read()

    with open('./queries/query_gather_triples.txt', 'r') as f:
        query_all_triples = f.read()

    save_folder = "./triples/"
    g = Graph()

    if not os.path.exists(os.path.join(save_folder, "train_set_movies")):
        os.makedirs(os.path.join(save_folder, "train_set_movies"))
    train_set_folder_movies = os.path.join(save_folder, "train_set_movies")

    if not os.path.exists(os.path.join(save_folder, "train_set_persons")):
        os.makedirs(os.path.join(save_folder, "train_set_persons"))
    train_set_folder_persons = os.path.join(save_folder, "train_set_persons")

    t = time.time()
    print("Querying all movies...")
    movie_data = deepcopy(query_wikidata(query=query_all_movies, timeout=120))
    print("Done in {:.2f}s".format(time.time() - t))

    t = time.time()
    print("Start gathering all individuals related to movies...")
    set_of_persons = gather_individuals(movie_data_list=movie_data)
    print("Done in {:.2f}s".format(time.time() - t))

    t = time.time()
    print("Gathering all movie triples...")
    g = gather_movie_triples(graph=g, movie_data_list=movie_data)
    print("Done in {:.2f}s".format(time.time() - t))

    t = time.time()
    print("Start gathering all individual triples...")
    gather_person_triples(graph=g, persons_set=set_of_persons)
    print("Done in {:.2f}s".format(time.time() - t))

    g.serialize(destination=os.path.join(save_folder, "store_base.nt"), format="nt")

    t = time.time()
    print("Solving set cover problem...")
    filtered_indices, index_to_genre, genre_to_index, all_genres, filtered_genres = solve_scp_problem(graph=g)
    print("Done in {:.2f}s".format(time.time() - t))

    t = time.time()
    print("Start removing redundant genres in movies...")
    g = removing_redundant_genres(graph=g,
                                  accepted_indices=filtered_indices,
                                  i2n=index_to_genre,
                                  genre_set=all_genres,
                                  accepted_genres=filtered_genres)
    print("Done in {:.2f}s".format(time.time() - t))

    t = time.time()
    print("Removing genres from other nodes than movies...")
    g = removing_other_genres(graph=g)
    print("Done in {:.2f}s".format(time.time() - t))

    g.serialize(destination=os.path.join(save_folder, "store_stripped.nt"), format="nt")

    g.parse(source=os.path.join(save_folder, "store_stripped.nt"), format="nt")

    t = time.time()
    print("Extracting images from movies with genres...")
    g = film_poster_extraction(graph=g, root_folder=train_set_folder_movies)
    print("Done in {:.2f}s".format(time.time() - t))

    t = time.time()
    print("Extracting images from individuals...")
    g = individual_photo_extraction(graph=g, root_folder=train_set_folder_persons)
    print("Done in {:.2f}s".format(time.time() - t))

    t = time.time()
    print("Adding images to knowledge graph...")
    g = add_images_to_graph(graph=g)
    print("Done in {:.2f}s".format(time.time() - t))

    t = time.time()
    print("Removing imdb ids from knowledge graph...")
    g = remove_imdb_id(graph=g)
    print("Done in {:.2f}s".format(time.time() - t))

    t = time.time()
    print("Creating splits...")
    create_training_validation_test_set(g, save_folder, train_set_folder_movies, train_set_folder_persons)
    print("Done in {:.2f}s".format(time.time() - t))

    g.serialize(destination=os.path.join(save_folder, "store_final.nt"), format="nt")
